-- This is a decorative tool to paint exposed surfaces in some basic colors
-- can be used to cover up unpleasant areas (e.g. cobblestone walls)
-- or to mark areas with colors (colored paths on floors, color lines on walls)
-- Colors are grouped together in 9 modes. The HEX values are taken from the dye
-- textures from dye mod of minetest_game. Within every mode, the colors are cycled from
-- brighter to darker hue on every subsequent application of the tool.

local S = technic.getter

local spray_painter_max_charge = 10000
local spray_painter_cpa = 10

local color_modes = {
	{name = S("Red"), index = 1, n = 2, ct = {"c91818", "730505"}, trampoline = {17, 18, 25}},
	{name = S("Yellow and Orange"), index = 3, n = 4, ct = {"fcf611", "ffc20b", "e0601a", "b52607"}, trampoline = {27, 30}},
	{name = S("Green and Dark Green"), index = 7, n = 4, ct = {"67eb1c", "4bb71c", "2b7b00", "154f00"}, trampoline = {0, 1, 2, 20, 31}},
	{name = S("Blue and Violet"), index = 11, n = 4, ct = {"00519d", "003376", "480680", "3b0367"}, trampoline = {5, 6, 7, 8, 10, 23, 24}},
	{name = S("Pink and Magenta"), index = 15, n = 4, ct = {"ffa5a5", "ff7272", "d80481", "a90145"}, trampoline = {16, 19}},
	{name = S("Cyan"), index = 19, n = 2, ct = {"00959d", "00676f"}, trampoline = {8, 9}},
	{name = S("Brown"), index = 21, n = 2, ct = {"6c3800", "391a00"}, trampoline = {3, 21, 25, 26, 28, 29}},
	{name = S("White and Grey"), index = 23, n = 4, ct = {"eeeeee", "b8b8b8", "9c9c9c", "5c5c5c"}, trampoline = {4, 10, 11, 12, 13, 14, 15}},
	{name = S("Dark Grey and Black"), index = 27, n = 4, ct = {"494949", "292929", "222222", "1b1b1b"}, trampoline = {15, 22, 23, 24}} -- 2 middle colors are swapped
}

-- Nodes to use the trampoline palette
technic.alternate_paintable_nodes = {
	["technic:trampoline"] = true,
	["xdecor:curtain_white"] = true,
	["xdecor:curtain_open_white"] = true,
}

local paint_layer = "technic:paint_layer"
local fluorescent_paint_layer = "technic:fluorescent_paint_layer"

minetest.register_node (paint_layer, {
	description = S("Paint"),
	drawtype = "nodebox",
	tiles = {"technic_paint.png"},
	node_box = {
		type = "wallmounted",
		wall_bottom = {-0.5, -0.5, -0.5, 0.5, -0.49, 0.5},
		wall_top = {-0.5, 0.49, -0.5, 0.5, 0.5, 0.5},
		wall_side = {-0.5, -0.5, -0.5, -0.49, 0.5, 0.5},
	},
	drop = "",
	groups = {attached_node = 1, dig_immediate = 2, not_in_creative_inventory = 1, not_blocking_trains = 1},
	paramtype = "light",
	paramtype2 = "colorwallmounted",
	palette = "technic_paint_palette.png",
})

minetest.register_node (fluorescent_paint_layer, {
	description = S("Fluorescent Paint"),
	drawtype = "nodebox",
	tiles = {"technic_paint.png"},
	node_box = {
		type = "wallmounted",
		wall_bottom = {-0.5, -0.5, -0.5, 0.5, -0.49, 0.5},
		wall_top = {-0.5, 0.49, -0.5, 0.5, 0.5, 0.5},
		wall_side = {-0.5, -0.5, -0.5, -0.49, 0.5, 0.5},
	},
	drop = "",
	groups = {attached_node = 1, dig_immediate = 2, not_in_creative_inventory = 1, not_blocking_trains = 1},
	light_source = 5,
	paramtype = "light",
	paramtype2 = "colorwallmounted",
	palette = "technic_paint_palette.png",
})


if minetest.get_modpath("ehlphabet") then
	minetest.register_node(":ehlphabet:block_color", {
		description = S("Ehlphabet Block (colored)"),
		tiles = {"ehlphabet_000.png"},
		groups = {cracky = 3, not_in_creative_inventory = 1},
		drop = "ehlphabet:block",
		paramtype = "light",
		paramtype2 = "colorwallmounted",
		palette = "technic_paint_palette.png",
	})

	minetest.register_node(":ehlphabet:block_color_fluorescent", {
		description = S("Ehlphabet Block (colored)"),
		tiles = {"ehlphabet_000.png"},
		groups = {cracky = 3, not_in_creative_inventory = 1},
		light_source = 7,
		drop = "ehlphabet:block",
		paramtype = "light",
		paramtype2 = "colorwallmounted",
		palette = "technic_paint_palette.png",
	})
end

do
	-- Apply palette index of itemstack, if set. This is needed because `place_param2`
	-- otherwise overrides unconditionally.
	local function paintable_after_place_node(pos, placer, itemstack, pointed_thing)
		local palette_index = tonumber(itemstack:to_table().meta.palette_index)
		if palette_index then
			local node = minetest.get_node(pos)
			node.param2 = palette_index
			minetest.swap_node(pos, node)
		end
	end

	-- Alter node definition to be paintable by the spray painter using the trampoline palette.
	function technic.make_alternate_paintable(nodename)
		local ndef = minetest.registered_nodes[nodename]
		assert(ndef, "Node does not exist: " .. nodename)

		local new_def = {}

		if not ndef.paramtype2 or ndef.paramtype2 == "none" then
			new_def.paramtype2 = "color"
		elseif ndef.paramtype2 == "4dir"
			or ndef.paramtype2 == "facedir"
			or ndef.paramtype2 == "wallmounted" then
			new_def.paramtype2 = "color" .. ndef.paramtype2
		else
			assert(false, "Unsupported paramtype2 in " .. nodename)
		end

		assert(not ndef.place_param2, "place_param2 would be overridden in " .. nodename)
		new_def.place_param2 = 88

		if not ndef.after_place_node then
			new_def.after_place_node = paintable_after_place_node
		else
			-- Wrap existing function
			local old_fun = ndef.after_place_node
			new_def.after_place_node = function(...)
				paintable_after_place_node(...)
				return old_fun(...)
			end
		end

		assert(not ndef.palette, "palette would be overridden in " .. nodename)
		new_def.palette = "technic_trampoline_palette.png"
	
		minetest.override_item(nodename, new_def, {})

		technic.alternate_paintable_nodes[nodename] = true
	end
end

local function spray_painter_setmode(user, itemstack, is_fluorescent)
	local player_name = user:get_player_name()

	local mode = itemstack:get_definition()._spray_painter_mode

	if mode == 0 then
		minetest.chat_send_player(player_name,
			S("Use while sneaking to change Spray Painter modes."))
	end

	mode = mode % 9 + 1

	local tool = is_fluorescent and "technic:fluorescent_spray_painter" or "technic:spray_painter"

	itemstack:set_name(tool .. "_" .. mode);
	minetest.chat_send_player(player_name,
		S("Spray Painter") .. ": " .. color_modes[mode].name)

	return itemstack
end

local function spray_paint(itemstack, user, pointed_thing, ptype)
	local keys = user:get_player_control()

	local mode = itemstack:get_definition()._spray_painter_mode

	if user.get_pos == nil then
		-- we are held in a node breaker and it will not work
		return itemstack
	end

	if mode == 0 or keys.sneak then
		return spray_painter_setmode(user, itemstack, ptype)
	end

	local charge = technic.get_RE_charge(itemstack)

	if charge < spray_painter_cpa then
		return itemstack
	end

	if pointed_thing.type ~= "node" then
		return itemstack
	end

	minetest.sound_play("technic_spray_painter", {
		pos = user:get_pos(),
		gain = 0.4,
	})

	-- player needs to own both the wall and its surface
	local pname = user:get_player_name()
	if minetest.is_protected(pointed_thing.under, pname) or
		minetest.is_protected(pointed_thing.above, pname) then
		minetest.record_protection_violation(pointed_thing.under, pname)
		return itemstack
	end

	local paint_name = ptype and fluorescent_paint_layer or paint_layer

	local target = minetest.get_node_or_nil(pointed_thing.under)

	local paintable = false

	-- target-specific code
	if target then

		-- if pointing at ehlphabet block (regular or colored)
		if (target.name == "ehlphabet:block" or target.name == "ehlphabet:block_color" or target.name == "ehlphabet:block_color_fluorescent") then

			if target.name == "ehlphabet:block" then
				if not ptype then
					minetest.swap_node(pointed_thing.under, { name = "ehlphabet:block_color" })
				else
					minetest.swap_node(pointed_thing.under, { name = "ehlphabet:block_color_fluorescent" })
				end
				target = minetest.get_node_or_nil(pointed_thing.under)
			end

			paintable = true

		-- if pointing at plastic blocks
		elseif minetest.get_item_group(target.name, "paintable_plastic_block") > 0 then
			paintable = true
		-- if the tool is pointed at a layer of paint -> cycling colors
		elseif target.name == paint_name then
			paintable = true
		end

	end

	if paintable then
		local p2 = target.param2
		local orientation = p2 % 8
		local cindex = (p2 - orientation) / 8
		local new_cindex = cindex + 1
		if new_cindex < color_modes[mode].index - 1 then
			new_cindex = color_modes[mode].index - 1
		end
		if new_cindex > color_modes[mode].index + (color_modes[mode].n - 1) - 1 then
			new_cindex = color_modes[mode].index - 1
		end

		minetest.swap_node(pointed_thing.under, {
									name = target.name,
									param2 = new_cindex*8 + orientation
									})

		if not technic.creative_mode then
			charge = charge - spray_painter_cpa
			technic.set_RE_charge(itemstack, charge)
		end

		return itemstack
	elseif target and technic.alternate_paintable_nodes[target.name] then
		local p2 = target.param2
		local orientation = p2 % 8
		local cindex = (p2 - orientation) / 8

		local itable = color_modes[mode].trampoline
		local new_cindex
		local current
		for i, e in ipairs(itable) do
			if e == cindex then
				current = i
				break
			end
		end
		if not current then
			new_cindex = itable[1]
		else
			new_cindex = itable[(current % #itable) + 1]
		end

		target.param2 = new_cindex*8 + orientation

		minetest.swap_node(pointed_thing.under, target)

		if not technic.creative_mode then
			charge = charge - spray_painter_cpa
			technic.set_RE_charge(itemstack, charge)
		end

		return itemstack
	end

	-- otherwise, spray some paint anew

	target = minetest.get_node_or_nil(pointed_thing.above)

	if not target or target.name ~= "air" then
		return itemstack
	end

	local diff = vector.subtract(pointed_thing.under, pointed_thing.above)
	local wdr = minetest.dir_to_wallmounted(diff)
	minetest.swap_node(pointed_thing.above, {
			name = paint_name,
			param2 = (color_modes[mode].index - 1) * 8 + wdr
	})

	if not technic.creative_mode then
		charge = charge - spray_painter_cpa
		technic.set_RE_charge(itemstack, charge)
	end
	return itemstack
end

technic.register_power_tool("technic:spray_painter", {
	description = S("Spray Painter"),
	inventory_image = "technic_spray_painter.png",
	stack_max = 1,
	technic_max_charge = spray_painter_max_charge,
	on_use = function(itemstack, user, pointed_thing)
		spray_paint(itemstack, user, pointed_thing, false)
		return itemstack
	end,

	_spray_painter_mode = 0
})


for i = 1, 9 do
	technic.register_power_tool("technic:spray_painter_" .. i, {
		description = S("Spray Painter") .. ": " .. color_modes[i].name,
		inventory_image = "technic_spray_painter.png^technic_tool_mode" .. i .. ".png",
		technic_max_charge = spray_painter_max_charge,
		wield_image = "technic_spray_painter.png",
		groups = {not_in_creative_inventory = 1},
		on_use = function(itemstack, user, pointed_thing)
			spray_paint(itemstack, user, pointed_thing, false)
			return itemstack
		end,

		_spray_painter_mode = i
	})
end


-- Provide a crafting recipe
local trigger = minetest.get_modpath("mesecons_button") and "mesecons_button:button_off"
	or "default:mese_crystal_fragment"

minetest.register_craft({
	output = 'technic:spray_painter',
	recipe = {
		{'pipeworks:tube_1', 'technic:stainless_steel_ingot', 'technic:battery'},
		{'', 'vessels:steel_bottle', trigger},
		{'dye:red', 'dye:green', 'dye:blue'},
	}
})


-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

technic.register_power_tool("technic:fluorescent_spray_painter", {
	description = S("Fluorescent Spray Painter"),
	inventory_image = "technic_spray_painter_fluorescent.png",
	stack_max = 1,
	technic_max_charge = spray_painter_max_charge,
	on_use = function(itemstack, user, pointed_thing)
		spray_paint(itemstack, user, pointed_thing, true)
		return itemstack
	end,

	_spray_painter_mode = 0
})


for i = 1, 9 do
	technic.register_power_tool("technic:fluorescent_spray_painter_" .. i, {
		description = S("Fluorescent Spray Painter") .. ": " .. color_modes[i].name,
		groups = {not_in_creative_inventory = 1},
		inventory_image = "technic_spray_painter_fluorescent.png^technic_tool_mode" .. i .. ".png",
		technic_max_charge = spray_painter_max_charge,
		wield_image = "technic_spray_painter_fluorescent.png",
		on_use = function(itemstack, user, pointed_thing)
			spray_paint(itemstack, user, pointed_thing, true)
			return itemstack
		end,

		_spray_painter_mode = i
	})
end


-- Provide a crafting recipe
local trigger = minetest.get_modpath("mesecons_button") and "mesecons_button:button_off"
	or "default:mese_crystal_fragment"

minetest.register_craft({
	output = 'technic:fluorescent_spray_painter',
	recipe = {
		{'pipeworks:tube_1', 'technic:stainless_steel_ingot', 'technic:battery'},
		{'technic:uranium_ingot', 'vessels:steel_bottle', trigger},
		{'dye:red', 'dye:green', 'dye:blue'},
	}
})
