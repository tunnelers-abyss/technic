local S = technic.getter

minetest.register_alias("sawmill", "technic:lv_sawmill")
minetest.register_craft({
	output = 'technic:lv_sawmill',
	recipe = {
		{'technic:stainless_steel_ingot', 'technic:diamond_drill_head', 'technic:stainless_steel_ingot'},
		{'technic:fine_copper_wire',      'technic:machine_casing',     'technic:motor'},
		{'technic:carbon_steel_ingot',    'technic:lv_cable',           'technic:carbon_steel_ingot'},
	},
})

technic.register_base_machine("technic:lv_sawmill", {
	typename = "sawmilling",
	description = S("@1 Sawmill", S("LV")),
	tier = "LV",
	demand = { 200 },
	speed = 3,
	node_box = {
		type = "fixed",
		fixed = {
			{ -3 / 32,  14 / 32, -1 / 32, 3 / 32,  15 / 32, 1 / 32 },
			{ -6 / 32,  13 / 32, -1 / 32, 6 / 32,  14 / 32, 1 / 32 },
			{ -8 / 32,  12 / 32, -1 / 32, 8 / 32,  13 / 32, 1 / 32 },
			{ -9 / 32,  11 / 32, -1 / 32, 9 / 32,  12 / 32, 1 / 32 },
			{ -10 / 32, 10 / 32, -1 / 32, 10 / 32, 11 / 32, 1 / 32 },
			{ -11 / 32, 9 / 32,  -1 / 32, 11 / 32, 10 / 32, 1 / 32 },
			{ -12 / 32, 7 / 32,  -1 / 32, 12 / 32, 9 / 32,  1 / 32 },
			{ -1 / 2,   -1 / 2,  -1 / 2,  1 / 2,   7 / 32,  1 / 2 },
		},
	},
	tiles = { nil, nil, nil, nil, "_back", nil },
})

minetest.after(0.1, function()
minetest.override_item("technic:lv_sawmill", {
    drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-3/32, 14/32, -1/32, 3/32, 15/32, 1/32},
			{-6/32, 13/32, -1/32, 6/32, 14/32, 1/32},
			{-8/32, 12/32, -1/32, 8/32, 13/32, 1/32},
			{-9/32, 11/32, -1/32, 9/32, 12/32, 1/32},
			{-10/32, 10/32, -1/32, 10/32, 11/32, 1/32},
			{-11/32, 9/32, -1/32, 11/32, 10/32, 1/32},
			{-12/32, 7/32, -1/32, 12/32, 9/32, 1/32},
			{-1/2, -1/2, -1/2, 1/2, 7/32, 1/2},
		},
	},
    tiles = {"technic_lv_sawmill_top.png","technic_lv_sawmill_bottom.png","technic_lv_sawmill_side.png","technic_lv_sawmill_side.png","technic_lv_sawmill_back.png","technic_lv_sawmill_front.png"},
})
end)

